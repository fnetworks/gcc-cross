#!/bin/bash
set -e

echo "BINUTILS: Getting sources"
wget "https://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS_VERSION.tar.gz"
tar -xzf "binutils-$BINUTILS_VERSION.tar.gz"
rm "binutils-$BINUTILS_VERSION.tar.gz"

mkdir "binutils-$BINUTILS_VERSION-build"
cd "binutils-$BINUTILS_VERSION-build"

echo "BINUTILS: Building"
../binutils-"$BINUTILS_VERSION"/configure --target="$TARGET" --prefix="$PREFIX" --disable-nls --disable-werror --with-sysroot
# shellcheck disable=SC2086
make $MAKE_PARALLELIZATION
make install-strip

echo "BINUTILS: Testing"
"$TARGET"-as --version
"$TARGET"-ld --version
