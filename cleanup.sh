#!/bin/bash

echo "Cleaning prefix in $PREFIX:"

echo " -> Removing documentation in '/share/info' and '/share/man'"
rm -r "$PREFIX/share/man" "$PREFIX/share/info"

echo " -> Removing bash completion (/etc/bash_completion.d)"
rm -r "$PREFIX/etc/bash_completion.d"

echo " -> Removing empty directories"
if [ ! "$(ls -A "$PREFIX/include")" ]; then
    echo "    - /include"
    rm -r "$PREFIX/include"
fi
