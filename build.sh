#!/bin/bash
set -e

# Build scripts
BINUTILS_SCRIPT=$PWD/binutils.sh
GCC_SCRIPT=$PWD/gcc.sh

# Configuration variables
export HOST=${HOST:-x86_64-pc-linux-gnu}
export TARGET=${TARGET:-i686-elf}
export PREFIX="$PWD/toolchain_${HOST}_${TARGET}"

if [ -z ${CI+x} ]; then # If $CI is not set
    echo "Not in CI"
    export MAKE_PARALLELIZATION="-j"
    # Multithreading only on local; CI doesn't need to be that fast
fi

# Builds could require some target executables in PATH
export PATH="$PATH:$PREFIX/bin"

# Default versions
export BINUTILS_VERSION=${BINUTILS_VERSION:-2.33.1}
export GCC_VERSION=${GCC_VERSION:-9.2.0}

echo "============================================================ "
echo " Building"
echo "  - binutils $BINUTILS_VERSION"
echo "  - gcc $GCC_VERSION"
echo " for host '$HOST', targeting '$TARGET'."
echo "============================================================ "
echo

mkdir -p build/
cd build/

bash "$BINUTILS_SCRIPT"
bash "$GCC_SCRIPT"

# Cleanup
./cleanup.sh

# Packaging
echo "Packaging toolchain ..."
PACKAGE_NAME=${1:-package.tar.gz}
tar -czf "$PACKAGE_NAME" "$PREFIX"