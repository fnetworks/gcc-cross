#!/bin/bash
set -e

echo "GCC: Getting sources"
wget "https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz"
tar -xzf "gcc-$GCC_VERSION.tar.gz"
rm "gcc-$GCC_VERSION.tar.gz"

echo "GCC: Downloading prerequisites"
pushd "gcc-$GCC_VERSION"
contrib/download_prerequisites
popd

mkdir "gcc-$GCC_VERSION-build"
cd "gcc-$GCC_VERSION-build"

echo "GCC: Building"
../gcc-"$GCC_VERSION"/configure --target="$TARGET" --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
# shellcheck disable=SC2086
make all-gcc $MAKE_PARALLELIZATION
make all-target-libgcc
make install-strip-gcc
make install-strip-target-libgcc
make install-strip-lto-plugin

echo "GCC: Testing"
"$TARGET"-gcc --version
"$TARGET"-g++ --version
