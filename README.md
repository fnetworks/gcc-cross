# Build scripts for GCC cross compilation

Currently included:

-   binutils 2.33.1
-   gcc 9.2.0

## Building

1. Make sure you have the required build dependencies (at least make, wget, gcc and g++).
2. Run `./build.sh [tar-file-name]`. This will build the toolchain, remove unnecessary files and
   then package it in a tar file in the `build` directory.
